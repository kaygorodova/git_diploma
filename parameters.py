from PyQt5.QtWidgets import QWidget, QButtonGroup
from PyQt5.uic import loadUi
from PyQt5.QtCore import pyqtSlot, pyqtSignal, Qt



class Parameters(QWidget):
    getParameters = pyqtSignal(bool, bool, float)

    def __init__(self, parent):
        super(Parameters, self).__init__(parent, Qt.Dialog)
        loadUi("interfaceParameters.ui", self)
        self.setWindowModality(Qt.WindowModal)


        self.coherence = QButtonGroup()
        self.coherence.addButton(self.pairedSamples)
        self.coherence.addButton(self.unpairedSamples)
        self.pairedSamples.setChecked(True)

        self.removal = QButtonGroup()
        self.removal.addButton(self.delRows)
        self.removal.addButton(self.notDelRows)
        self.delRows.setChecked(True)

        self.pairedSamples.toggled.connect(self.pairedSamplesSlot)
        self.useParameters.clicked.connect(self.useParametersSlot)

        #self.getParameters = pyqtSignal()


    @pyqtSlot(bool)
    def pairedSamplesSlot(self, checked):
    	self.delRows.setEnabled(checked)
    	self.notDelRows.setEnabled(checked)
    	pass

    @pyqtSlot()
    def useParametersSlot(self):
        self.hide()
    	self.getParameters.emit(self.pairedSamples.isChecked(),
            self.delRows.isChecked(), self.statisticalSignificance.value())
        self.close()
    	pass


