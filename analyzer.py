# -*- coding: utf-8 -*-
import pandas as pd
import numpy as np
from types import *
from enum import Enum
from scipy import stats
from statsmodels.sandbox.stats import runs

from PyQt5.QtCore import pyqtSlot, Qt
from PyQt5.QtWidgets import QMessageBox


from result import Result


class Analyzer():

    def __init__(self):

        self.dataType = Enum('dataType', 'quantitative ordinal nominal binary')

        self.dictionary = pd.DataFrame()

        self.samples = pd.DataFrame()

        self.samplesCount = int()
        self.coherence = bool()
        self.statisticalSignificance = float()
        self.normality = bool()
        self.typeSamples = []

        self.criterions = []


    def setSamples(self, samples):
        self.samples = samples
        self.samplesCount = len(samples.columns)


    def warning(self, str):
        self.message = QMessageBox()
        self.message.setWindowTitle(u'Предупреждение')
        self.message.setText(str)
        self.message.setWindowModality(Qt.ApplicationModal)
        self.message.exec_()


    @pyqtSlot(bool, bool, float)
    def analysis(self, coherence, removal, statisticalSignificance):
        self.coherence = coherence and removal
        self.statisticalSignificance = statisticalSignificance * 0.01

        if coherence:
            self.samples = self.samples.dropna()


        if self.typeSamples == self.dataType.quantitative:
            self.normalityDefinition()


        if self.typeSamples[0] == self.dataType.ordinal:
            self.ranking()

        if self.typeSamples[0] == self.dataType.nominal\
        or self.typeSamples[0] == self.dataType.binary\
        and self.typeSamples[1] == self.dataType.nominal:
            self.convertNominalData()

        self.selectMethods()
        self.useMethods()
        self.outResult()
        pass

    def validityDefinition(self):
        rows, columns = self.samples.shape
        if self.samplesCount < 2:
            self.warning(u'Число сравниваемых выборок меньше 2!')
            return False

        if not rows:
            self.warning(u'Сравниваемые выборки пусты')
            return False

        for i in xrange(columns):
            if not self.samples[self.samples.columns[i]].count():
                self.warning('Среди сравниваемых выборок есть пустые')
                return False

        if not self.typeDefinitionSamples():
            self.warning(u'Сравниваемые выборки имеют разный тип')
            return False

        return True

    def normalityDefinition(self):
        self.normality = True

        for col in self.samples.columns:
            if col.dropna().size < 20:
                self.normality = False
                break

            x, pval = stats.normaltest(col)
            if pval < 0.05:
                self.normality = False
                break




    def typeDefinitionSamples(self):
        rows, columns = self.samples.shape
        self.typeSamples = self.typeDefinition(self.samples[self.samples.columns[0]].dropna())

        for i in xrange(1, columns):
            typeSample = self.typeDefinition(self.samples[self.samples.columns[i]].dropna())

            if self.typeSamples == typeSample:
                continue

            if self.typeSamples[0] == self.dataType.binary and self.typeSamples[1] == typeSample[0]:
                self.typeSamples = typeSample
                continue

            if typeSample[0] == self.dataType.binary and typeSample[1] == self.typeSamples[0]:
                continue

            if self.typeSamples[0] == self.dataType.binary and typeSample[0] == self.dataType.binary and self.typeSamples[1] == typeSample[1]:
                self.typeSamples = [self.typeSamples[1]]
                continue

            return False
        return True





    def typeDefinition(self, sample):

        def isinstanceFloat(x):
            return isinstance(x, float)

        def isinstanceInt(x):
            return isinstance(x, int)


        sampleSet = set(sample)
        for index, row in self.dictionary.iterrows():
            if sampleSet <= set(row):
                return [self.dataType.ordinal, index]

        numberFloat = len(filter(isinstanceFloat, sample))
        numberInt = len(filter(isinstanceInt, sample))
        if numberFloat + numberInt == sample.count():
            if sample.nunique() == 2:
                return [self.dataType.binary, self.dataType.quantitative, sampleSet]
            return [self.dataType.quantitative]


        if sample.nunique() == 2:
            return [self.dataType.binary, self.dataType.nominal, sampleSet]


        return [self.dataType.nominal]

    def ranking(self):
        dataSet = self.dictionary.iloc[self.typeSamples[1]].dropna()
        invertedDataSet = pd.Series(map(int, dataSet.index),
            dataSet.as_matrix())


        for row, s in self.samples.iterrows():
            for col in self.samples.columns:
                cell = self.samples.get_value(row, col)
                if cell == cell:
                    self.samples.set_value(row, col, invertedDataSet[cell])


    def convertNominalData(self):
        dic = dict()
        i = 0
        for row, s in self.samples.iterrows():
            for col in self.samples.columns:
                cell = self.samples.get_value(row, col)
                if cell == cell:
                    if not cell in dic:
                        dic[cell] = i
                        i += 1
                    self.samples.set_value(row, col, dic[cell])


    def selectMethods(self):

        print self.typeSamples, self.normality, self.coherence, self.samplesCount
        #1
        if self.typeSamples[0] == self.dataType.quantitative and\
        self.normality and self.coherence and self.samplesCount == 2:
            self.criterions = [self.signtest, self.wilcoxon, self.chi2,
            self.studentPair]
            return

        #2
        if (self.typeSamples[0] == self.dataType.quantitative or\
        self.typeSamples[0] == self.dataType.ordinal) and self.coherence and\
        self.samplesCount != 2:
            self.criterions = [self.friedman, self.paget, self.chi2]
            return

        #3
        if self.typeSamples[0] == self.dataType.quantitative and\
        self.normality and not self.coherence and self.samplesCount == 2:
            self.criterions = [self.rosenbaum, self.mannwhitneyu, self.chi2,
            self.studentInd, self.phisher]
            return


        #4
        if (self.typeSamples[0] == self.dataType.quantitative or\
        self.typeSamples[0] == self.dataType.ordinal) and not self.coherence and\
        self.samplesCount != 2:
            self.criterions = [self.kruskalwallis, self.dzhonkir, self.chi2]
            return

        #5
        if (self.typeSamples[0] == self.dataType.quantitative and\
        not self.normality or self.typeSamples[0] == self.dataType.ordinal)\
        and self.coherence and self.samplesCount == 2:
            self.criterions = [self.signtest, self.wilcoxon, self.chi2]
            return

        #6
        if (self.typeSamples[0] == self.dataType.quantitative and\
        not self.normality or self.typeSamples[0] == self.dataType.ordinal) and\
        not self.coherence and self.samplesCount == 2:
            self.criterions = [self.rosenbaum, self.mannwhitneyu, self.chi2]
            return


        #7
        if self.typeSamples[0] == self.dataType.binary and self.coherence and\
        self.samplesCount == 2:
            self.criterions = [self.mcnemar, self.chi2]
            return

        #8
        self.criterions = [self.chi2]



    def studentInd(self):
        samples = self.samples.as_matrix().transpose()
        t, pvalue = stats.ttest_ind(samples[0], samples[1])
        self.significanceOfdifferences = pvalue <= self.statisticalSignificance
        self.setStrResult(u't-критерий Стьюдента для несвязных выборок', t, pvalue)

    def studentPair(self):
        samples = self.samples.as_matrix().transpose()
        t, pvalue = stats.ttest_rel(samples[0], samples[1])
        self.significanceOfdifferences = pvalue <= self.statisticalSignificance
        self.setStrResult(u't-критерий Стьюдента для связных выборок', t, pvalue)


    def phisher(self):
        samples = self.samples.as_matrix().transpose()
        f, pvalue = stats.f_oneway(*samples)
        self.significanceOfdifferences = pvalue <= self.statisticalSignificance
        self.setStrResult(u'критерием Фишера', f, pvalue)

    def signtest(self):
        return

    def wilcoxon(self):
        rows, columns = self.samples.shape
        if rows < 20:
            return
        samples = self.samples.as_matrix().transpose()
        t, pvalue = stats.wilcoxon(samples[0], samples[1])
        self.significanceOfdifferences = pvalue <= self.statisticalSignificance
        self.setStrResult(u'Т-критерий Вилкоксона', t, pvalue)

    def mcnemar(self):
        samples = self.samples.as_matrix().transpose()
        stat, pvalue = runs.mcnemar(samples[0], samples[1])
        self.significanceOfdifferences = pvalue <= self.statisticalSignificance
        self.setStrResult(u'Критерий Макнамары', stat, pvalue)

    def mannwhitneyu(self):
        for col in self.samples.columns:
            if self.samples[col].dropna().size < 20:
                return

        samples = self.samples.as_matrix().transpose()
        u, prob = stats.mannwhitneyu(samples[0], samples[1])
        self.significanceOfdifferences = 2 * prob <= self.statisticalSignificance
        self.setStrResult(u'U-критерий Манна-Уитни', u, 2 * prob)


    def rosenbaum(self):
        return



    def chi2(self):
        samples = self.samples.as_matrix().transpose()
        matrix = []
        dic = {}
        index = 0
        for i in xrange(len(samples)):
            vec = stats.itemfreq(samples[i])
            for j in vec:
                if not j[0] in dic:
                    dic[j[0]] = index
                    index += 1
                    matrix.append([0 for g in xrange(len(samples))])
                matrix[dic[j[0]]][i] = int(j[1])


        chi, pvalue, dof, expected = stats.chi2_contingency(np.array(matrix).transpose())
        self.significanceOfdifferences = pvalue <= self.statisticalSignificance
        self.setStrResult(u'Критерий хи-квадрат', chi, pvalue)



    def friedman(self):
        samples = self.samples.as_matrix().transpose()
        f, pvalue = stats.friedmanchisquare(*samples)
        self.significanceOfdifferences = pvalue <= self.statisticalSignificance
        self.setStrResult(u'Критерий Фридмана', f, pvalue)


    def kruskalwallis(self):
        samples = self.samples.as_matrix().transpose()
        h, pvalue = stats.kruskal(*samples)
        self.significanceOfdifferences = pvalue <= self.statisticalSignificance
        self.setStrResult(u'Критерий Краскела-Уоллиса', h, pvalue)


    def paget(self):
        return

    def dzhonkir(self):
        return

    def useMethods(self):
        self.significanceOfdifferences = False
        self.strResult = ['', '']
        print self.criterions

        for f in self.criterions:
            f()
            if self.significanceOfdifferences:
                break

    def outResult(self):

        text = u'Названия анализируемых выборок: '
        for col in self.samples.columns:
            text += col + u' '
        text +=  u'\n\n'

        if self.strResult == ['', '']:
            self.warning(u'Маленький объем выборок')
            return

        if self.strResult[0] != u'':
            text += self.strResult[0] + u'\n'

        if self.significanceOfdifferences:
            text += self.strResult[1]


        self.result = Result()
        self.result.setModal(True)
        self.result.text.setText(text)

        self.result.close.clicked.connect(self.closeResult)
        self.result.defaultFileName += '_'.join(self.samples.columns) + u'.txt'

        self.result.show()

    def setStrResult(self, nameTest, testStatistic, pvalue):
        text = self.createStrResult(nameTest, testStatistic, pvalue)
        if self.significanceOfdifferences:
            self.strResult[1] = text
        else:
            self.strResult[0] = text


    def createStrResult(self, nameTest, testStatistic, pvalue):
        text = u'Критерий использованный для анализа: '
        text += nameTest + u'\n'
        text += u'Результат анализа: '
        if self.significanceOfdifferences:
            text += u'Исследуемые выборки имеют достоверные статистические различия\n'
        else:
            text += u'Исследуемые выборки не имеют достоверных статистических различий\n'
        text += u'Тестовая статистика: '
        text += str(testStatistic)
        text += u' Значимая вероятность: '
        text += str(pvalue) + u'\n'

        return text



    def setDictionary(self, dictionary):
        for i, row in dictionary.iterrows():
            if row.count() != len(set(row.dropna())):
                self.warning(u'В одном из наборов есть повторяющиеся альтернативы')
                return
            if row.count() < 3:
                self.warning(u'Число альтернатив в одном из наборов меньше 3')
                return
        self.dictionary = dictionary


    @pyqtSlot()
    def closeResult(self):
        del self.result



