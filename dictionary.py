# -*- coding: utf-8 -*-
import sys
import pandas as pd
from PyQt5.uic import loadUi
from PyQt5.QtWidgets import QWidget, QTableWidgetItem, QFileDialog, QHeaderView
from PyQt5.QtCore import pyqtSlot, Qt

class Dictionary(QWidget):
    def __init__(self, parent):
        super(Dictionary, self).__init__(parent, Qt.Dialog)
        loadUi("interfaceDictionary.ui", self)
        self.setWindowModality(Qt.WindowModal)
        self.modal = True
        self.data = pd.DataFrame()
        self.fileName = ()


        self.rowCount = 1
        self.columnCount = 3

        self.activeColumn = -1
        self.activeRow = -1

        self.addColumn.clicked.connect(self.addColumnSlot)
        self.addRow.clicked.connect(self.addRowSlot)

        self.delColumn.clicked.connect(self.delColumnSlot)
        self.delRow.clicked.connect(self.delRowSlot)

        self.saveDictionary.clicked.connect(self.saveDictionarySlot)
        self.saveDictionaryAs.clicked.connect(self.saveDictionaryAsSlot)

        self.imageDictionary.cellActivated.connect(self.setActiveCell)
        self.imageDictionary.cellChanged.connect(self.cellChangedSlot)

    def createDictionary(self):
        self.fileName = ()
        self.setShapeOfDictionary()

        d = {}
        self.data = pd.DataFrame(d, index = range(0, self.rowCount),
            columns = map(str, range(1, self.columnCount + 1)))


    def setShapeOfDictionary(self):
        self.imageDictionary.clear()
        self.imageDictionary.setRowCount(self.rowCount)
        self.imageDictionary.setColumnCount(self.columnCount)

        self.delRow.setEnabled(False)
        self.delColumn.setEnabled(False)

        for i in xrange(self.columnCount):
            header = QTableWidgetItem(u'Ранг ' + str(i + 1))
            self.imageDictionary.setHorizontalHeaderItem(i, header)

        for i in xrange(self.rowCount):
            header = QTableWidgetItem(u'Набор ' + str(i + 1))
            self.imageDictionary.setVerticalHeaderItem(i, header)

        self.imageDictionary.horizontalHeader().resizeSections(QHeaderView.ResizeToContents)




    def openDictionary(self, dictionaryName):
        self.fileName = dictionaryName
        self.data = pd.read_csv(dictionaryName[0])
        self.outDictionary()



    def outDictionary(self):
        self.rowCount, self.columnCount = self.data.shape

        self.setShapeOfDictionary()

        self.imageDictionary.blockSignals(True)

        for i in xrange(self.rowCount):
            for j in xrange(self.columnCount):
                if self.data.iget_value(i, j) == self.data.iget_value(i, j):
                    cell = QTableWidgetItem(str(self.data.iget_value(i, j)))
                    self.imageDictionary.setItem(i, j, cell)

        self.imageDictionary.blockSignals(False)
        self.imageDictionary.horizontalHeader().resizeSections(QHeaderView.ResizeToContents)

    @pyqtSlot(int, int)
    def setActiveCell(self, row, column):
        self.activeRow = row
        self.activeColumn = column
        self.delColumn.setEnabled(self.columnCount > 3)
        self.delRow.setEnabled(self.rowCount > 1)
        pass


    @pyqtSlot(int)
    def rowsCountChange(self, number):
        self.rowCount = number
        pass

    @pyqtSlot(int)
    def columnsCountChange(self, number):
        self.columnCount = number
        pass

    @pyqtSlot()
    def addRowSlot(self):
        self.imageDictionary.insertRow(self.rowCount)
        header = QTableWidgetItem(u'Набор ' + str(self.rowCount + 1))
        self.imageDictionary.setVerticalHeaderItem(self.rowCount, header)

        d = {}
        row = pd.DataFrame(d, index = range(self.rowCount, self.rowCount + 1),
            columns = self.data.columns.values)
        self.data = self.data.append(row)
        self.rowCount += 1
        self.delRow.setEnabled(self.rowCount > 1 and self.activeRow != -1)
        pass

    @pyqtSlot()
    def addColumnSlot(self):
        self.imageDictionary.insertColumn(self.columnCount)
        header = QTableWidgetItem(u'Ранг ' + str(self.columnCount + 1))
        self.imageDictionary.setHorizontalHeaderItem(self.columnCount, header)

        d = {}
        column = pd.DataFrame(d, index = range(0, self.rowCount),
            columns = [0])
        self.data.insert(self.columnCount, str(self.columnCount + 1), column[0])
        self.columnCount += 1
        self.delColumn.setEnabled(self.columnCount > 3 and self.activeColumn != -1)
        pass


    @pyqtSlot()
    def delRowSlot(self):
        if self.activeRow == -1:
            return
        self.imageDictionary.removeRow(self.activeRow)
        self.rowCount -= 1

        for i in xrange(self.activeRow, self.rowCount):
            header = QTableWidgetItem(u'Набор ' + str(i + 1))
            self.imageDictionary.setVerticalHeaderItem(i, header)

        self.data = self.data.drop([self.activeRow], axis = 0)
        self.data.index = xrange(self.rowCount)

        self.activeRow = -1

        self.delRow.setEnabled(False)
        pass


    @pyqtSlot()
    def delColumnSlot(self):
        if self.activeColumn == -1:
            return

        self.imageDictionary.removeColumn(self.activeColumn)
        self.columnCount -= 1

        for i in xrange(self.activeColumn, self.columnCount):
            header = QTableWidgetItem(u'Ранг ' + str(i + 1))
            self.imageDictionary.setHorizontalHeaderItem(i, header)


        self.data = self.data.drop([str(self.activeColumn + 1)], axis = 1)

        self.data.columns = map(str, xrange(1, self.columnCount + 1))

        self.activeColumn = -1

        self.delColumn.setEnabled(False)
        pass


    @pyqtSlot()
    def saveDictionarySlot(self):
        if self.fileName:
            self.data.to_csv(self.fileName[0], index=False,  encoding='utf-8')
        else:
            self.saveDictionaryAsSlot()
        pass


    @pyqtSlot()
    def saveDictionaryAsSlot(self):
        self.fileName = QFileDialog.getSaveFileName(self,
            u'Сохранить словарь как...', './', '*.csv')

        if self.fileName[0]:
            self.data.to_csv(self.fileName[0], index=False, encoding='utf-8')
        pass


    @pyqtSlot(int, int)
    def cellChangedSlot(self, row, column):
        value = self.imageDictionary.cellWidget(row, column).text()
        columns = self.data.columns
        self.data.set_value(row, columns[column], value)
        self.imageDictionary.horizontalHeader().resizeSections(QHeaderView.ResizeToContents)



