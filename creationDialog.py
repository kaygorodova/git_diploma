from PyQt5.QtWidgets import QDialog
from PyQt5.uic import loadUi



class CreationDialog(QDialog):
    def __init__(self):                                                         
        super(CreationDialog, self).__init__()                                    
        loadUi("interfaceCreationDialog.ui", self) 
