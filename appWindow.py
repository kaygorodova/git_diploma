# -*- coding: utf-8 -*-
from PyQt5.QtCore import QCoreApplication, pyqtSlot, Qt
from PyQt5.uic import loadUi
from PyQt5.QtWidgets import QMainWindow, QFileDialog, QTableWidgetItem, QCheckBox
from PyQt5.QtWidgets import QWidget, QHBoxLayout, QHeaderView

from analyzer import Analyzer
import pandas as pd
from creationDialog import CreationDialog
from dictionary import Dictionary
from parameters import Parameters

class AppWindow(QMainWindow):

    def __init__(self):
        super(AppWindow, self).__init__()
        loadUi("interfaceAppWindow.ui", self)

        self.fileName = ()
        #self.table = Analyzer()
        self.data = pd.DataFrame()
        self.tableCheckBoxes = []
        self.rowCount = int()
        self.columnCount = int()

        self.analyzer = Analyzer()

        self.activeColumn = -1
        self.activeRow = -1


        self.createFile.triggered.connect(self.createFileSlot)
        self.openFile.triggered.connect(self.openFileSlot)
        self.saveFile.triggered.connect(self.saveFileSlot)
        self.saveFileAs.triggered.connect(self.saveFileAsSlot)
        self.quit.triggered.connect(QCoreApplication.quit)


        self.createDictionary.triggered.connect(self.createDictionarySlot)
        self.useDictionary.triggered.connect(self.useDictionarySlot)
        self.editDictionary.triggered.connect(self.editDictionarySlot)

        self.imageTable.cellChanged.connect(self.cellChangedSlot)


        self.addRow.clicked.connect(self.addRowSlot)
        self.addColumn.clicked.connect(self.addColumnSlot)
        self.delRow.clicked.connect(self.delRowSlot)
        self.delColumn.clicked.connect(self.delColumnSlot)
        self.analysis.clicked.connect(self.analysisSlot)

        self.imageTable.cellActivated.connect(self.setActiveCell)



    @pyqtSlot()
    def createFileSlot(self):
        self.createFileDialog = CreationDialog()
        self.createFileDialog.setModal(True)

        self.rowCount = self.createFileDialog.rowCount.value()
        self.columnCount = self.createFileDialog.columnCount.value()

        self.createFileDialog.accepted.connect(self.createTable)
        self.createFileDialog.rowCount.valueChanged.connect(self.setRowCount)
        self.createFileDialog.columnCount.valueChanged.connect(self.setColumnCount)

        self.createFileDialog.show()
        pass

    @pyqtSlot()
    def createTable(self):
        self.fileName = ()
        d = {}
        self.data = pd.DataFrame(d, index = range(0, self.rowCount),
            columns = map(str, range(1, self.columnCount + 1)))
        self.setShapeOfTable()
        pass

    def setShapeOfTable(self):
        self.imageTable.clear()
        self.tableCheckBoxes = []
        self.activeTableCheckBoxes = 0
        #self.fileName.clear()
        self.imageTable.setRowCount(self.rowCount + 1)
        self.imageTable.setColumnCount(self.columnCount)

        namesColumns = self.data.columns.values

        for i in xrange(self.columnCount):
            cellWidget = QWidget()
            cellCheckBox = QCheckBox()
            self.tableCheckBoxes.append(cellCheckBox)
            self.tableCheckBoxes[-1].stateChanged.connect(self.stateChangedSlot)
            #cellLayout = QHBoxLayout(cellWidget)
            cellLayout = QHBoxLayout()
            cellLayout.addWidget(cellCheckBox)
            cellLayout.setAlignment(Qt.AlignCenter)
            cellLayout.setContentsMargins(0, 0, 0, 0)
            cellWidget.setLayout(cellLayout)
            self.imageTable.setCellWidget(0, i, cellWidget)


            header = QTableWidgetItem(namesColumns[i])
            self.imageTable.setHorizontalHeaderItem(i, header)


        self.imageTable.verticalHeader().hide()


        self.saveFile.setEnabled(True)
        self.saveFileAs.setEnabled(True)
        self.delColumn.setEnabled(False)
        self.delRow.setEnabled(False)
        self.addColumn.setEnabled(True)
        self.addRow.setEnabled(True)
        self.analysis.setEnabled(True)

    @pyqtSlot(int)
    def setRowCount(self, rowCount):
        self.rowCount = rowCount
        pass

    @pyqtSlot(int)
    def setColumnCount(self, columnCount):
        self.columnCount = columnCount
        pass


    @pyqtSlot()
    def openFileSlot(self):
        fileTypes = "*.xls;;*.xlsx;;*.csv"
        self.fileName = QFileDialog.getOpenFileName(self, u'Открыть файл', './', fileTypes)
        if self.fileName[0]:
            #self.table.readData(self.fileName)
            if self.fileName[1] == u'*.csv':
                self.data = pd.read_csv(self.fileName[0])
            else:
                self.data = pd.read_excel(self.fileName[0], 0)

            self.rowCount, self.columnCount = self.data.shape

            while self.rowCount < 1:
                self.addRowSlot()
                pass
            while self.columnCount < 2:
                self.addColumnSlot()
                pass

            columns = map(unicode, self.data.columns)
            for i in xrange(self.columnCount):
                if columns[i][:7] == u'Unnamed':
                    columnName = i + 1
                    while str(columnName) in columns:
                        columnName += 1
                    columns[i] = str(columnName)
            self.data.columns = columns

            self.outTable()
        pass


    def outTable(self):
        #data = self.table.data
        self.rowCount, self.columnCount = self.data.shape
        self.setShapeOfTable()

        self.imageTable.blockSignals(True)

        for i in xrange(self.rowCount):
            for j in xrange(self.columnCount):
                if self.data.iget_value(i, j) == self.data.iget_value(i, j):
                    cell = QTableWidgetItem(str(self.data.iget_value(i, j)))
                    self.imageTable.setItem(i + 1, j, cell)


        self.imageTable.blockSignals(False)

        self.imageTable.horizontalHeader().resizeSections(QHeaderView.ResizeToContents)


    @pyqtSlot()
    def saveFileSlot(self):
        if self.fileName:
            self.saveData()
        else:
            self.saveFileAsSlot()
        pass

    def saveData(self):
        if self.fileName[1] == u'*.csv':
            self.data.to_csv(self.fileName[0], index=False,  encoding='utf-8')
        else:
            ew = pd.ExcelWriter(self.fileName[0], encoding='utf-8')
            self.data.to_excel(ew, index=False)
            ew.save()


    @pyqtSlot()
    def saveFileAsSlot(self):

        fileTypes = "*.xls;;*.xlsx;;*.csv"
        self.fileName = QFileDialog.getSaveFileName(self, u'Сохранить файл как...',
            './', fileTypes)

        if self.fileName[1] in ['*.xls', '*.xlsx', '*.csv']:
            self.fileName = [self.fileName[0] + u'.xls', u'*.xls']


        if self.fileName[0]:
            self.saveData()
        pass

    @pyqtSlot()
    def createDictionarySlot(self):
        self.dialog = CreationDialog()
        self.child = Dictionary(self)



        self.dialog.columnCount.setMinimum(3)
        self.dialog.setModal(True)
        self.dialog.labelRowCount.setText(u'Число наборов')
        self.dialog.labelColumnCount.setText(u'Число рангов')
        self.dialog.show()
        self.dialog.accepted.connect(self.outDictionary)
        self.dialog.rowCount.valueChanged.connect(self.child.rowsCountChange)
        self.dialog.columnCount.valueChanged.connect(self.child.columnsCountChange)
        pass

    @pyqtSlot()
    def outDictionary(self):
        #self.child.setWindowModality(Qt.ApplicationModal)
        self.child.show()
        self.child.createDictionary()
        self.child.useDictionary.clicked.connect(self.setDictionary)
        pass

    @pyqtSlot()
    def setDictionary(self):
        self.analyzer.setDictionary(self.child.data)
        pass


    @pyqtSlot()
    def useDictionarySlot(self):
        dictionaryName = QFileDialog.getOpenFileName(self, u'Использовать словарь', './', '*.csv')
        if dictionaryName[0]:
            dictionary = pd.read_csv(dictionaryName[0])
            self.analyzer.setDictionary(dictionary)

    @pyqtSlot()
    def editDictionarySlot(self):
        self.child = Dictionary(self)
        dictionaryName = QFileDialog.getOpenFileName(self, u'Редактировать словарь', './', '*.csv')
        if dictionaryName[0]:
            self.child.openDictionary(dictionaryName)
            self.child.show()
            self.child.useDictionary.clicked.connect(self.setDictionary)

    @pyqtSlot(int)
    def stateChangedSlot(self, state):
        if state:
            self.activeTableCheckBoxes += 1
            if self.activeTableCheckBoxes == 6:
                for i in self.tableCheckBoxes:
                    i.setEnabled(i.isChecked())
        else:
            self.activeTableCheckBoxes -= 1
            if self.activeTableCheckBoxes == 5:
                for i in self.tableCheckBoxes:
                    i.setEnabled(True)


    @pyqtSlot()
    def analysisSlot(self):
        sampleNumbers = []
        for i in xrange(self.columnCount):
            if self.tableCheckBoxes[i].isChecked():
                sampleNumbers.append(i)

        samples = self.data.iloc[:, sampleNumbers]
        samples = samples.dropna(how='all')

        self.analyzer.setSamples(samples)
        if not self.analyzer.validityDefinition():
            return

        self.param = Parameters(self)
        self.param.getParameters.connect(self.analyzer.analysis)
        self.param.show()


    @pyqtSlot(int, int)
    def cellChangedSlot(self, row, column):
        if row == 0:
            return
        value = self.getCellValue(row, column)
        columns = self.data.columns
        self.data.set_value(row - 1, columns[column], value)
        self.imageTable.horizontalHeader().resizeSections(QHeaderView.ResizeToContents)
        pass


    def getCellValue(self, row, column):
        string = self.imageTable.cellWidget(row, column).text()
        if string == '':
            return string
        point = False
        for i in string:
            if not i in ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']:
                if i != '.' or i == '.' and point:
                    return string
                point = True

        if point:
            return float(string)
        return int(string)

    @pyqtSlot(int, int)
    def setActiveCell(self, row, column):
        if row != 0:
            self.activeRow = row
            self.delRow.setEnabled(self.rowCount > 1)
        else:
            self.activeRow = -1
            self.delRow.setEnabled(False)
        self.activeColumn = column
        self.delColumn.setEnabled(self.columnCount > 2)
        pass

    @pyqtSlot()
    def addRowSlot(self):
        self.imageTable.insertRow(self.rowCount + 1)

        d = {}
        row = pd.DataFrame(d, index = range(self.rowCount, self.rowCount + 1),
            columns = self.data.columns.values)
        self.data = self.data.append(row)
        self.rowCount += 1

        #self.table.addRow()

        self.delRow.setEnabled(self.rowCount > 1 and self.activeRow != -1)
        pass

    @pyqtSlot()
    def addColumnSlot(self):
        self.imageTable.insertColumn(self.columnCount)

        d = {}
        column = pd.DataFrame(d, index = range(0, self.rowCount),
            columns = [0])

        columnName = self.columnCount + 1
        while str(columnName) in self.data.columns:
            columnName += 1
        self.data.insert(self.columnCount, str(columnName), column[0])


        self.columnCount += 1

        cellWidget = QWidget()
        cellCheckBox = QCheckBox()
        self.tableCheckBoxes.append(cellCheckBox)
        #cellLayout = QHBoxLayout(cellWidget)
        cellLayout = QHBoxLayout()
        cellLayout.addWidget(cellCheckBox)
        cellLayout.setAlignment(Qt.AlignCenter)
        cellLayout.setContentsMargins(0, 0, 0, 0)
        cellWidget.setLayout(cellLayout)
        self.imageTable.setCellWidget(0, self.columnCount - 1, cellWidget)

        header = QTableWidgetItem(str(columnName))
        self.imageTable.setHorizontalHeaderItem(self.columnCount - 1, header)

        #self.table.addColumn()

        self.delColumn.setEnabled(self.columnCount > 2 and self.activeColumn != -1)
        pass


    @pyqtSlot()
    def delRowSlot(self):
        if self.activeRow == -1:
            return
        self.imageTable.removeRow(self.activeRow)

        self.data = self.data.drop([self.activeRow - 1], axis=0)
        self.rowCount -= 1
        self.data.index = xrange(self.rowCount)


        self.activeColumn = -1

        self.delRow.setEnabled(False)
        pass


    @pyqtSlot()
    def delColumnSlot(self):
        if self.activeColumn == -1:
            return

        self.imageTable.removeColumn(self.activeColumn)
        self.data = self.data.drop([self.data.columns[self.activeColumn]], axis=1)
        self.columnCount -= 1

        del self.tableCheckBoxes[self.activeColumn]


        self.activeColumn = -1

        self.delColumn.setEnabled(False)
        pass




