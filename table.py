# -*- coding: utf-8 -*-
import pandas as pd
import numpy as np
from types import *
from enum import Enum
from scipy import stats
from statsmodels.sandbox.stats import runs

from PyQt5.QtCore import pyqtSlot, Qt
from PyQt5.QtWidgets import QMessageBox


from result import Result


class Table():

    def __init__(self):

        self.dataType = Enum('dataType', 'quantitative ordinal nominal binary')

        self.data = pd.DataFrame()
        self.rowCount = int()
        self.columnCount = int()

        self.dictionary = pd.DataFrame()

        self.samples = pd.DataFrame()

        self.samplesCount = int()
        self.coherence = bool()
        self.statisticalSignificance = float()
        self.normality = bool()
        self.typeSamples = []

        self.criterions = []

        #self.dictionary = pd.read_csv('./d.csv')


    def readData(self, fileName):
        if fileName[1] == u'*.csv':
            self.data = pd.read_csv(fileName[0])
        else:
            self.data = pd.read_excel(fileName[0], 0)
        self.rowCount, self.columnCount = self.data.shape


    def saveData(self, fileName):
        if fileName[1] == u'*.csv':
            self.data.to_csv(fileName[0], index=False,  encoding='utf-8')
        else:
            self.data.to_excel(fileName[0], index=False,  encoding='utf-8')



    def setShape(self, rowCount, columnCount):
        self.rowCount = rowCount
        self.columnCount = columnCount
        d = {}
        self.data = pd.DataFrame(d, index = range(0, rowCount),
            columns = range(0, columnCount))

    def setSamples(self, sampleNumbers):
        self.samplesCount = len(sampleNumbers)
        self.samples = self.data.iloc[:, sampleNumbers]
        self.samples = self.samples.dropna(how='all')

    def cellChanged(self, row, column, value):
        columns = self.data.columns
        self.data.set_value(row, columns[column], value)

    def addRow(self):
        d = {}
        row = pd.DataFrame(d, index = range(self.rowCount, self.rowCount + 1),
            columns = self.data.columns.values)
        self.data = self.data.append(row)
        self.rowCount += 1

    def addColumn(self):
        column = [float('NaN') for i in xrange(self.rowCount)]
        columnName = self.columnCount
        while columnName in self.data.columns:
            columnName += 1
        self.data.insert(self.columnCount, columnName, column)
        self.columnCount += 1

    def delRow(self, activeRow):
        self.data = self.data.drop([activeRow], axis=0)
        self.rowCount -= 1
        self.data.index = xrange(self.rowCount)



    def delColumn(self, activeColumn):
        self.data = self.data.drop([self.data.columns[activeColumn]], axis=1)
        self.columnCount -= 1



    def warning(self, str):
        self.message = QMessageBox()
        self.message.setWindowTitle(u'Предупреждение')
        self.message.setText(str)
        self.message.setWindowModality(Qt.ApplicationModal)
        self.message.exec_()


    @pyqtSlot(bool, bool, float)
    def analys(self, coherence, removal, statisticalSignificance):
        self.coherence = coherence and removal
        self.statisticalSignificance = statisticalSignificance * 0.01

        if coherence:
            self.samples = self.samples.dropna()

        rows, columns = self.samples.shape

        if not rows:
            self.warning(u'Сравниваемые выборки пусты')
            return

        for i in xrange(columns):
            if not self.samples[self.samples.columns[i]].count():
                self.warning('Среди сравниваемых выборок есть пустые')
                return

        if not self.typeDefinitionSamples():
            self.warning(u'Сравниваемые выборки имеют разный тип')
            return



        if self.typeSamples == self.dataType.quantitative:
            self.normality = True

            for col in self.samples.columns:
                if col.dropna().size < 20:
                    self.normality = False
                    break
                x, pval = stats.normaltest(col)
                if pval < 0.05:
                    self.normality = False
                    break



        if self.typeSamples[0] == self.dataType.ordinal:
            self.ranking()

        if self.typeSamples[0] == self.dataType.nominal\
        or self.typeSamples[0] == self.dataType.binary\
        and self.typeSamples[1] == self.dataType.nominal:
            self.convertNominalData()

        self.selectMethods()
        self.useMethods()
        self.outResult()
        pass

    def typeDefinitionSamples(self):
        rows, columns = self.samples.shape
        self.typeSamples = self.typeDefinition(self.samples[self.samples.columns[0]].dropna())

        for i in xrange(1, columns):
            typeSample = self.typeDefinition(self.samples[self.samples.columns[i]].dropna())

            if self.typeSamples == typeSample:
                continue

            if self.typeSamples[0] == self.dataType.binary and self.typeSamples[1] == typeSample[0]:
                self.typeSamples = typeSample
                continue

            if typeSample[0] == self.dataType.binary and typeSample[1] == self.typeSamples[0]:
                continue

            if self.typeSamples[0] == self.dataType.binary and typeSample[0] == self.dataType.binary and self.typeSamples[1] == typeSample[1]:
                self.typeSamples = [self.typeSamples[1]]
                continue


            return False
        return True





    def typeDefinition(self, sample):

        def isinstanceFloat(x):
            return isinstance(x, float)

        def isinstanceInt(x):
            return isinstance(x, int)


        sampleSet = set(sample)
        for index, row in self.dictionary.iterrows():
            if sampleSet <= set(row):
                return [self.dataType.ordinal, index]

        numberFloat = len(filter(isinstanceFloat, sample))
        numberInt = len(filter(isinstanceInt, sample))
        if numberFloat + numberInt == sample.count():
            if sample.nunique() == 2:
                return [self.dataType.binary, self.dataType.quantitative, sampleSet]
            return [self.dataType.quantitative]



        if sample.nunique() == 2:
            return [self.dataType.binary, self.dataType.nominal, sampleSet]


        return [self.dataType.nominal]

    def ranking(self):
        dataSet = self.dictionary.iloc[self.typeSamples[1]].dropna()
        invertedDataSet = pd.Series(map(int, dataSet.index),
            dataSet.as_matrix())


        for row, s in self.samples.iterrows():
            for col in self.samples.columns:
                cell = self.samples.get_value(row, col)
                if cell == cell:
                    self.samples.set_value(row, col, invertedDataSet[cell])


    def convertNominalData(self):
        dic = dict()
        i = 0
        for row, s in self.samples.iterrows():
            for col in self.samples.columns:
                cell = self.samples.get_value(row, col)
                if cell == cell:
                    if not cell in dic:
                        dic[cell] = i
                        i += 1
                    self.samples.set_value(row, col, dic[cell])


    def selectMethods(self):

        print self.typeSamples, self.normality, self.coherence, self.samplesCount
        #1
        if self.typeSamples[0] == self.dataType.quantitative and\
        self.normality and self.coherence and self.samplesCount == 2:
            self.criterions = [self.signtest, self.wilcoxon, self.studentPair]
            return

        #2
        if (self.typeSamples[0] == self.dataType.quantitative or\
        self.typeSamples[0] == self.dataType.ordinal) and self.coherence and\
        self.samplesCount != 2:
            self.criterions = [self.friedman, self.paget]
            return

        #3
        if self.typeSamples[0] == self.dataType.quantitative and\
        self.normality and not self.coherence and self.samplesCount == 2:
            self.criterions = [self.rosenbaum, self.mannwhitneyu, self.studentInd,
            self.phisher]
            return


        #4
        if (self.typeSamples[0] == self.dataType.quantitative or\
        self.typeSamples[0] == self.dataType.ordinal) and not self.coherence and\
        self.samplesCount != 2:
            self.criterions = [self.kruskalwallis, self.dzhonkir]
            return

        #5
        if (self.typeSamples[0] == self.dataType.quantitative and\
        not self.normality or self.typeSamples[0] == self.dataType.ordinal)\
        and self.coherence and self.samplesCount == 2:
            self.criterions = [self.signtest, self.wilcoxon]
            return

        #6
        if self.typeSamples[0] == self.dataType.quantitative and\
        not self.normality and not self.coherence and self.samplesCount == 2:
            self.criterions = [self.rosenbaum, self.mannwhitneyu]
            return

        #7
        if self.typeSamples[0] == self.dataType.ordinal and not self.coherence\
        and self.samplesCount == 2:
            self.criterions = [self.rosenbaum]
            return

        #8
        if self.typeSamples[0] == self.dataType.binary and self.coherence and\
        self.samplesCount == 2:
            self.criterions = [self.mcnemar]
            return



        #9
        self.criterions = [self.chi2]



    def studentInd(self):
        samples = self.samples.as_matrix().transpose()
        t, pvalue = stats.ttest_ind(samples[0], samples[1])
        self.significanceOfdifferences = pvalue <= self.statisticalSignificance
        self.setStrResult(u't-критерий Стьюдента для несвязных выборок', t, pvalue)

    def studentPair(self):
        samples = self.samples.as_matrix().transpose()
        t, pvalue = stats.ttest_rel(samples[0], samples[1])
        self.significanceOfdifferences = pvalue <= self.statisticalSignificance
        self.setStrResult(u't-критерий Стьюдента для связных выборок', t, pvalue)


    def phisher(self):
        samples = self.samples.as_matrix().transpose()
        f, pvalue = stats.f_oneway(*samples)
        self.significanceOfdifferences = pvalue <= self.statisticalSignificance
        self.setStrResult(u'критерием Фишера', f, pvalue)

    def signtest(self):
        return

    def wilcoxon(self):
        rows, columns = self.samples.shape
        if rows < 20:
            return
        samples = self.samples.as_matrix().transpose()
        t, pvalue = stats.wilcoxon(samples[0], samples[1])
        self.significanceOfdifferences = pvalue <= self.statisticalSignificance
        self.setStrResult(u'Т-критерий Вилкоксона', t, pvalue)

    def mcnemar(self):
        samples = self.samples.as_matrix().transpose()
        stat, pvalue = runs.mcnemar(samples[0], samples[1])
        self.significanceOfdifferences = pvalue <= self.statisticalSignificance
        self.setStrResult(u'Критерий Макнамары', stat, pvalue)

    def mannwhitneyu(self):
        for col in self.samples.columns:
            if self.samples[col].dropna().size < 20:
                return

        samples = self.samples.as_matrix().transpose()
        u, prob = stats.mannwhitneyu(samples[0], samples[1])
        self.significanceOfdifferences = 2 * prob <= self.statisticalSignificance
        self.setStrResult(u'U-критерий Манна-Уитни', u, 2 * prob)


    def rosenbaum(self):
        return



    def chi2(self):
        samples = self.samples.as_matrix().transpose()
        matrix = []
        dic = {}
        index = 0
        for i in xrange(len(samples)):
            vec = stats.itemfreq(samples[i])
            for j in vec:
                if not j[0] in dic:
                    dic[j[0]] = index
                    index += 1
                    matrix.append([0 for g in xrange(len(samples))])
                matrix[dic[j[0]]][i] = int(j[1])


        chi, pvalue, dof, expected = stats.chi2_contingency(np.array(matrix).transpose())
        self.significanceOfdifferences = pvalue <= self.statisticalSignificance
        self.setStrResult(u'Критерий хи-квадрат', chi, pvalue)



    def friedman(self):
        samples = self.samples.as_matrix()
        f, pvalue = stats.friedmanchisquare(*samples)
        self.significanceOfdifferences = pvalue <= self.statisticalSignificance
        self.setStrResult(u'Критерий Фридмана', f, pvalue)


    def kruskalwallis(self):
        samples = self.samples.as_matrix().transpose()
        h, pvalue = stats.kruskal(*samples)
        self.significanceOfdifferences = pvalue <= self.statisticalSignificance
        self.setStrResult(u'Критерий Краскела-Уоллиса', h, pvalue)


    def paget(self):
        return

    def dzhonkir(self):
        return

    def useMethods(self):
        self.significanceOfdifferences = False
        self.strResult = ['', '']
        print self.criterions

        for f in self.criterions:
            f()
            if self.significanceOfdifferences:
                break

    def outResult(self):

        text = u'Названия анализируемых выборок: '
        for col in self.samples.columns:
            text += col + u' '
        text +=  u'\n\n'

        if self.strResult == ['', '']:
            self.warning(u'Маленький объем выборок')
            return

        if self.strResult[0] != u'':
            text += self.strResult[0] + u'\n'

        if self.significanceOfdifferences:
            text += self.strResult[1]


        self.result = Result()
        self.result.setModal(True)
        self.result.text.setText(text)

        self.result.close.clicked.connect(self.closeResult)

        self.result.show()

    def setStrResult(self, nameTest, testStatistic, pvalue):
        text = self.createStrResult(nameTest, testStatistic, pvalue)
        if self.significanceOfdifferences:
            self.strResult[1] = text
        else:
            self.strResult[0] = text


    def createStrResult(self, nameTest, testStatistic, pvalue):
        text = u'Критерий использованный для анализа: '
        text += nameTest + u'\n'
        text += u'Результат анализа: '
        if self.significanceOfdifferences:
            text += u'Исследуемые выборки имеют достоверные статистические различия\n'
        else:
            text += u'Исследуемые выборки не имеют достоверных статистических различий\n'
        text += u'Тестовая статистика: '
        text += str(testStatistic)
        text += u' Значимая вероятность: '
        text += str(pvalue) + u'\n'

        return text



    def readDictionary(self, dictionaryName):
        self.dictionary = pd.read_csv(dictionaryName)

    @pyqtSlot()
    def closeResult(self):
        del self.result



