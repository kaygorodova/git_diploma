import sys
from PyQt5.QtWidgets import QApplication
from PyQt5.QtCore import QTranslator
from appWindow import AppWindow

def main():
    app = QApplication(sys.argv)
    translator = QTranslator()
    translator.load("./qt_ru.qm")
    app.installTranslator(translator)
    form = AppWindow()

    form.show()
    sys.exit(app.exec_())

if __name__ == '__main__':
    main()
